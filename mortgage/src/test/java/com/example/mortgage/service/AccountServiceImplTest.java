package com.example.mortgage.service;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.mortgage.dto.EmiRequestDto;
import com.example.mortgage.dto.EmiResponseDto;
import com.example.mortgage.entity.Transaction;
import com.example.mortgage.exception.MortgageException;
import com.example.mortgage.repository.TransactionRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AccountServiceImplTest {

	@InjectMocks
	AccountServiceImpl accountServiceImpl;

	@Mock
	TransactionRepository transactionRepository;

	@Test
	public void accountServiveImplTest() throws MortgageException {
		EmiRequestDto emiRequestDto = new EmiRequestDto();
		emiRequestDto.setPropertyCost(150000d);
		emiRequestDto.setInitialDeposit(30000d);
		emiRequestDto.setRateOfInterest(12d);
		emiRequestDto.setTenure(12);

		EmiResponseDto emiResponseDto = new EmiResponseDto();
		emiResponseDto.setMessage("success");
		emiResponseDto.setStatusCode(607);
		emiResponseDto.setEmiAmount(11183.561643835616);

		EmiResponseDto result = accountServiceImpl.emiCalculation(emiRequestDto);
		assertNotNull(result);

	}

	/*
	 * @Test(expected = MortgageException.class) public void
	 * accountServiveImplTest1() throws MortgageException {
	 * 
	 * EmiRequestDto emiRequestDto = new EmiRequestDto();
	 * emiRequestDto.setPropertyCost(150000d);
	 * emiRequestDto.setInitialDeposit(3000d); emiRequestDto.setRateOfInterest(12d);
	 * emiRequestDto.setTenure(12); Double fixedInitialCost =
	 * emiRequestDto.getPropertyCost() * 0.2;
	 * accountServiceImpl.emiCalculation(emiRequestDto);
	 * 
	 * }
	 */

	@Test(expected = MortgageException.class)
	public void accountServiveImplTest2() throws MortgageException {
		EmiRequestDto emiRequestDto = new EmiRequestDto();
		emiRequestDto.setPropertyCost(150000d);
		emiRequestDto.setInitialDeposit(30000d);
		emiRequestDto.setRateOfInterest(12d);
		emiRequestDto.setTenure(10);

		accountServiceImpl.emiCalculation(emiRequestDto);
	}

	@Test
	public void historyTest() throws MortgageException {
		Transaction transaction = new Transaction();
		transaction.setAccountNumber(123455L);
		transaction.setEmiAmount(1234d);
		transaction.setTransactionId(1);
		transaction.setTransactionType("CREDIT");

		List<Transaction> trans = new ArrayList<Transaction>();
		trans.add(transaction);

		Mockito.when(transactionRepository.findByAccountNumber(Mockito.anyLong())).thenReturn(Optional.of(trans));
		List<Transaction> result = accountServiceImpl.history(Mockito.anyLong());
		assertNotNull(result);
	}

	@Test(expected = MortgageException.class)
	public void historyTestNegative() throws MortgageException {
		Mockito.when(transactionRepository.findByAccountNumber(Mockito.anyLong()))
				.thenReturn(Optional.ofNullable(null));
		accountServiceImpl.history(Mockito.anyLong());

	}
}
