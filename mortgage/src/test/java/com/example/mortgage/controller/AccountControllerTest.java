package com.example.mortgage.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.mortgage.dto.EmiRequestDto;
import com.example.mortgage.dto.EmiResponseDto;
import com.example.mortgage.entity.Transaction;
import com.example.mortgage.exception.MortgageException;
import com.example.mortgage.service.AccountService;

@RunWith(MockitoJUnitRunner.class)
public class AccountControllerTest {

	@InjectMocks
	AccountController accountController;

	@Mock
	AccountService accountService;

	@Test
	public void emiCalculationTest() throws MortgageException {

		EmiRequestDto emiRequestDto = new EmiRequestDto();
		emiRequestDto.setPropertyCost(150000d);
		emiRequestDto.setInitialDeposit(30000d);
		emiRequestDto.setRateOfInterest(12d);
		emiRequestDto.setTenure(12);

		EmiResponseDto emiResponseDto = new EmiResponseDto();
		emiResponseDto.setMessage("success");
		emiResponseDto.setStatusCode(607);
		emiResponseDto.setEmiAmount(11183.561643835616);

		Mockito.when(accountService.emiCalculation(Mockito.any())).thenReturn(emiResponseDto);
		ResponseEntity<EmiResponseDto> actual = accountController.emiCalculation(emiRequestDto);
		assertNotNull(actual);

	}

	@Test
	public void historyTest() throws MortgageException {
		Transaction transaction = new Transaction();
		transaction.setAccountNumber(123455L);
		transaction.setEmiAmount(1234d);
		transaction.setTransactionId(1);
		transaction.setTransactionType("auto");

		List<Transaction> trans = new ArrayList<Transaction>();
		trans.add(transaction);

		Mockito.when(accountService.history(Mockito.anyLong())).thenReturn(trans);
		ResponseEntity<List<Transaction>> result = accountController.history(Mockito.anyLong());
		assertEquals(HttpStatus.OK, result.getStatusCode());
	}

}
