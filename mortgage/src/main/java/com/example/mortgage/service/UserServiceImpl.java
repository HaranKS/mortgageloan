package com.example.mortgage.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.example.mortgage.constants.ApplicationConstants;
import com.example.mortgage.dto.AccountDto;
import com.example.mortgage.dto.CustomerResponseDto;
import com.example.mortgage.dto.LoginRequestDto;
import com.example.mortgage.dto.LoginResponseDto;
import com.example.mortgage.dto.SignUpRequestDto;
import com.example.mortgage.dto.SignUpResponseDto;
import com.example.mortgage.entity.Account;
import com.example.mortgage.entity.Customer;
import com.example.mortgage.entity.Mortgage;
import com.example.mortgage.entity.Transaction;
import com.example.mortgage.exception.MortgageException;
import com.example.mortgage.repository.AccountRepository;
import com.example.mortgage.repository.CustomerRepository;
import com.example.mortgage.repository.MortgageRepository;
import com.example.mortgage.repository.TransactionRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	MortgageRepository mortgageRepository;

	@Autowired
	TransactionRepository transactionRepository;

	Random random = new Random();

	@Override
	public SignUpResponseDto signUp(SignUpRequestDto signUpRequestDto) throws MortgageException {
		log.info("INSIDE SIGNUP SERVICE!!!");

		SignUpResponseDto signUpResponseDto = null;
		Account savingsAccount = null;
		Account mortgageAccount = null;
		Customer customer = null;
		Mortgage mortgage = null;
		Transaction transaction = null;
		Transaction mortgageTransaction = null;
		LocalDate birthDay = signUpRequestDto.getDob();
		log.info("birthDay  " + signUpRequestDto.getDob());

		if (signUpRequestDto.getPropertyCost() >= 100000 && signUpRequestDto.getEmiAmount() > 0) {
			if (validIntialCost(signUpRequestDto.getPropertyCost(), signUpRequestDto.getInitialDeposit())) {
				if (validEMIAmount(signUpRequestDto.getSalary(), signUpRequestDto.getEmiAmount())) {
					if (validAge(birthDay)) {
						customer = new Customer();
						mortgage = new Mortgage();
						mortgageAccount = new Account();
						savingsAccount = new Account();
						transaction = new Transaction();
						mortgageTransaction = new Transaction();
						signUpResponseDto = new SignUpResponseDto();
						CustomerResponseDto customerResponseDto = new CustomerResponseDto();

						customer.setLoginId(signUpRequestDto.getCustomerName() + random.nextInt(1000));
						customer.setPassword(signUpRequestDto.getCustomerName() + "@" + random.nextInt(100));
						customer.setCustomerName(signUpRequestDto.getCustomerName());
						customer.setDob(birthDay);
						customer.setEmailId(signUpRequestDto.getEmailId());
						customer.setEmployementStatus(signUpRequestDto.getEmployementStatus());
						customer.setOccupation(signUpRequestDto.getOccupation());
						customer.setPanNumber(signUpRequestDto.getPanNumber());
						customer.setSalary(signUpRequestDto.getSalary());
						Customer customerRepo = customerRepository.save(customer);

						mortgage.setCustomerId(customerRepo.getCustomerId());
						mortgage.setEmiAmount(signUpRequestDto.getEmiAmount());
						mortgage.setInitialDeposit(signUpRequestDto.getInitialDeposit());
						mortgage.setOperationType(signUpRequestDto.getOperationType());
						mortgage.setPropertyCost(signUpRequestDto.getPropertyCost());
						mortgage.setRateOfInterest(signUpRequestDto.getRateOfInterest());
						mortgage.setTenure(signUpRequestDto.getTenure());
						mortgageRepository.save(mortgage);

						savingsAccount
								.setBalance(signUpRequestDto.getPropertyCost() - signUpRequestDto.getInitialDeposit());
						long range = 123456789L;
						long number = (long)(random.nextDouble()*range);
						log.info("Saving Account number----"+number);
						savingsAccount.setAccountNumber(number);
						savingsAccount.setAccountType(ApplicationConstants.SAVING_ACCOUNT);
						savingsAccount.setCreatedDate(LocalDate.now());
						savingsAccount.setCustomerId(customerRepo.getCustomerId());
						accountRepository.save(savingsAccount);

						mortgageAccount.setBalance(
								-(signUpRequestDto.getPropertyCost() - signUpRequestDto.getInitialDeposit()));
						long range1 = 9875654312L;
						long number1 = (long)(random.nextDouble()*range1);
						mortgageAccount.setAccountNumber(number1);
						mortgageAccount.setAccountType(ApplicationConstants.MORTGAGE_ACCOUNT);
						mortgageAccount.setCreatedDate(LocalDate.now());
						mortgageAccount.setCustomerId(customerRepo.getCustomerId());
						accountRepository.save(mortgageAccount);

						transaction.setAccountNumber(savingsAccount.getAccountNumber());
						transaction.setTransactionType(ApplicationConstants.DEBIT);
						transaction.setEmiAmount(signUpRequestDto.getEmiAmount());
						transaction.setTransactionDate(LocalDate.now());
						transactionRepository.save(transaction);

						mortgageTransaction.setAccountNumber(mortgageAccount.getAccountNumber());
						mortgageTransaction.setTransactionType(ApplicationConstants.CREDIT);
						mortgageTransaction.setEmiAmount(signUpRequestDto.getEmiAmount());
						mortgageTransaction.setTransactionDate(LocalDate.now());
						transactionRepository.save(mortgageTransaction);

						customerResponseDto.setLoginId(customer.getLoginId());
						customerResponseDto.setPassword(customer.getPassword());
						signUpResponseDto.setCustomerDto(customerResponseDto);
						signUpResponseDto.setMessage(ApplicationConstants.MORTGAGE_APPROVED_MESSAGE);
						signUpResponseDto.setStatusCode(ApplicationConstants.SUCCESS_CODE);
					} else {
						throw new MortgageException(ApplicationConstants.CUSTOMER_AGE_MINIMUM_MESSAGE);
					}
				} else {
					throw new MortgageException(ApplicationConstants.PROPER_EMI_AMOUNT);
				}
			} else {
				throw new MortgageException(ApplicationConstants.PROPER_INITIAL_AMOUNT);
			}
		} else {
			throw new MortgageException(ApplicationConstants.PROPERTY_COST_VALIDATION_MESSAGE);
		}
		return signUpResponseDto;
	}

	/**
     * @author Bairavi
     *
     *         Method is used for login, the Customer can login after registration with the loginId and password
     *
     * @param loginRequestDto that includes loginId and password
     * @return LoginResponseDto that includes custom status code, message, CustomerId, CustomerName, AccountNum,
     *          AccountType, and Balance
     *
     * @throws CustomerException
     */
	@Override
	public LoginResponseDto userLogin(LoginRequestDto loginRequestDto) throws MortgageException {

		LoginResponseDto loginResponseDto = new LoginResponseDto();
		Optional<Customer> user = customerRepository.findByLoginIdAndPassword(loginRequestDto.getLoginId(),
				loginRequestDto.getPassword());
		if (!user.isPresent()) {
			throw new MortgageException(ApplicationConstants.LOGIN_FAILURE);
		}

		loginResponseDto.setStatusCode(ApplicationConstants.SUCCESS_CODE);
		loginResponseDto.setMessage(ApplicationConstants.LOGIN_SUCCESS);
		loginResponseDto.setCustomerId(user.get().getCustomerId());
		loginResponseDto.setCustomerName(user.get().getCustomerName());

		Optional<List<Account>> account = accountRepository.findByCustomerId(loginResponseDto.getCustomerId());
		if(!account.isPresent()) {
			throw new MortgageException(ApplicationConstants.ACCOUNT_NOT_REGISTERED);
		}
		List<AccountDto> accountDto1 = new ArrayList<>();
		account.get().forEach(customerId -> {
			AccountDto accountDto = new AccountDto();
			accountDto.setAccountNumber(customerId.getAccountNumber());
			accountDto.setAccountType(customerId.getAccountType());
			accountDto.setBalance(customerId.getBalance());
			accountDto1.add(accountDto);
		});
		loginResponseDto.setAccountDto(accountDto1);
		return loginResponseDto;
	}

	/**
	 * This method is intended for batch process for every 10sec
	 * 
	 */
	public String batchProcess() {
		log.info("batch process time:{}", LocalDateTime.now());
		
		List<Account> getAllAccounts = getAllAccounts();
		if (!(getAllAccounts.isEmpty())) {
			Integer previousCustomerId = 0;
			Integer currentCustomerId = 0;
			for (Account account : getAllAccounts) {
				Integer customerId = account.getCustomerId();
				currentCustomerId = customerId;
				if (!currentCustomerId.equals(previousCustomerId)) {
					Account savingsAccount = getAccount(customerId, ApplicationConstants.SAVING_ACCOUNT);
					Account mortgageAccount = getAccount(customerId, ApplicationConstants.MORTGAGE_ACCOUNT);
					Mortgage emi = mortgageRepository.findByCustomerId(savingsAccount.getCustomerId());
					Double emiAmount = emi.getEmiAmount();
					log.info("EMIAMOUNT---- "+emiAmount);
					if (savingsAccount.getBalance() >= emiAmount || mortgageAccount.getBalance() < 0) {
						Double savingsAccountBalance = savingsAccount.getBalance() - emiAmount;
						Double mortgageAccountBalance = mortgageAccount.getBalance() + emiAmount;
						savingsAccount.setBalance(savingsAccountBalance);
						mortgageAccount.setBalance(mortgageAccountBalance);
						accountRepository.save(savingsAccount);
						accountRepository.save(mortgageAccount);

						Transaction transactionInSavings = new Transaction();
						Transaction transactionInMortgage = new Transaction();

						transactionInSavings.setAccountNumber(savingsAccount.getAccountNumber());
						transactionInSavings.setEmiAmount(emiAmount);
						transactionInSavings.setTransactionDate(LocalDate.now());
						transactionInSavings.setTransactionType(ApplicationConstants.DEBIT);

						transactionInMortgage.setAccountNumber(mortgageAccount.getAccountNumber());
						transactionInMortgage.setEmiAmount(emiAmount);
						transactionInMortgage.setTransactionDate(LocalDate.now());
						transactionInMortgage.setTransactionType(ApplicationConstants.CREDIT);

						transactionRepository.save(transactionInSavings);
						transactionRepository.save(transactionInMortgage);

						previousCustomerId = currentCustomerId;
					}
				}
			}

			return ApplicationConstants.AUTO_DEBIT_MONTHLY_SUCCESS_MESSAGE;
		} else {
			return ApplicationConstants.AUTO_DEBIT_MONTHLY_FAILED_MESSAGE;
		}
	}

	public List<Account> getAllAccounts() {
		return accountRepository.findAll();
	}

	public Account getAccount(Integer customerId, String accountType) {
		return accountRepository.findByCustomerIdAndAccountType(customerId, accountType);

	}
	
	@Scheduled(fixedRate = 60000)
	public void testSchedule() {
		batchProcess();
	}

	static boolean validAge(LocalDate date1) {
		boolean result = false;
		int birthYear = date1.getYear();
		int year = Calendar.getInstance().get(Calendar.YEAR);
		int age = year - birthYear;
		if (age > 21) {
			result = true;
		}
		return result;
	}

	static boolean validIntialCost(Double costOfProperty, Double initialDeposit) {
		boolean result = false;
		double fixedInitialCost = (costOfProperty*0.2);
		if (initialDeposit > fixedInitialCost ) {  
			result = true;
		}
		return result;
	}

	static boolean validEMIAmount(Double salary, Double emiAmount) {
		boolean result = false;
		double amount = 0.8 * salary;
		if (emiAmount <= amount) {
			result = true;
		}
		return result;
	}

}
