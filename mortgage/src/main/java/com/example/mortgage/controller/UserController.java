package com.example.mortgage.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.mortgage.dto.LoginRequestDto;
import com.example.mortgage.dto.LoginResponseDto;
import com.example.mortgage.dto.SignUpRequestDto;
import com.example.mortgage.dto.SignUpResponseDto;
import com.example.mortgage.exception.MortgageException;
import com.example.mortgage.service.UserService;

@RestController
@RequestMapping("/users")
@CrossOrigin(allowedHeaders = { "*", "*/" }, origins = { "*", "*/" })
public class UserController {

	@Autowired
	UserService userService;

	@PostMapping("")
	public ResponseEntity<SignUpResponseDto> signUp(@Valid @RequestBody SignUpRequestDto signUpRequestDto)
			throws MortgageException {
		return new ResponseEntity<>(userService.signUp(signUpRequestDto), HttpStatus.OK);
	}

	/**
     * @author Bairavi
     *
     *         Method is used for login, the Customer can login after registration with the loginId and password
     *
     * @param loginRequestDto that includes loginId and password
     * @return LoginResponseDto that includes custom status code, message, CustomerId, CustomerName, AccountNum,
     *          AccountType, and Balance
     *
     * @throws MortgageException
     */
	@PostMapping("/login")
	public ResponseEntity<LoginResponseDto> userLogin(@Valid @RequestBody LoginRequestDto loginRequestDto)
			throws MortgageException {
		return new ResponseEntity<>(userService.userLogin(loginRequestDto), HttpStatus.OK);
	}

}
