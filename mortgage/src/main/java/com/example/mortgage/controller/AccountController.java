package com.example.mortgage.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.mortgage.dto.EmiRequestDto;
import com.example.mortgage.dto.EmiResponseDto;
import com.example.mortgage.entity.Transaction;
import com.example.mortgage.exception.MortgageException;
import com.example.mortgage.service.AccountService;

@RestController
@RequestMapping("/account")
@CrossOrigin(allowedHeaders = { "*", "*/" }, origins = { "*", "*/" })
public class AccountController {

	@Autowired
	AccountService accountService;

	/**
     * This method is used to calculate the emi based on the given fields
     *
     * @author Haran
     * @since 2020-03-31
     * @body We are sending the propertyCost,initialDeposit,rateOfInterest and tenure to calculate emiAmount
     * @return We are getting the Emiamount based on the given fields along with
     *         status code and message
     * @throws AccountException
     */
	@PostMapping("")
	public ResponseEntity<EmiResponseDto> emiCalculation(@Valid @RequestBody EmiRequestDto emiRequestDto)
			throws MortgageException {
		return new ResponseEntity<>(accountService.emiCalculation(emiRequestDto), HttpStatus.OK);
	}
	 /**
     * @author Bairavi
     *
     *         Method is used to get the Transaction History based on the accountNumber
     *
     * @param  PathVariable is accountNumber
     * @return List<Transaction> that includes AccountNumber, EmiAmount, TransactionDate, TransactionId, and
     *         TransactionType
     *
     * @throws MortgageException
     */
	@GetMapping("/{accountNumber}")
    public ResponseEntity<List<Transaction>> history(@PathVariable(name ="accountNumber") Long accountNumber)
            throws MortgageException {
        return new ResponseEntity<>(accountService.history(accountNumber), HttpStatus.OK);
    }

}
