package com.example.mortgage.constants;

public class ApplicationConstants {

	private ApplicationConstants() {

	}

	public static final Integer SUCCESS_CODE = 607;
	public static final Integer UNSUCCESS_CODE = 608;
	public static final String MORTGAGE_APPROVED_MESSAGE = "CONGRATULATIONS!!! MORTGAGE IS GRANTED...";
	public static final String CUSTOMER_AGE_MINIMUM_MESSAGE = "YOU ARE NOT ELIGIBLE FOR THE LOAN AS YOUR AGE IS LESS THAN 21!!!";
	public static final String PROPERTY_COST_VALIDATION_MESSAGE = "PROPERT SHOULD BE MINIMUM OF 100000 OR EMI AMOUNT SHOULD NOT BELESS THAN 0!!!.";
	public static final String SAVING_ACCOUNT = "SAVING ACCOUNT";
	public static final String MORTGAGE_ACCOUNT = "MORTGAGE ACCOUNT";
	public static final String DEBIT = "DEBIT";
	public static final String CREDIT = "CREDIT";
	public static final String DATE_FORMAT = "YYYY-MM-DD";
	public static final String LOGIN_FAILURE = "PLEASE ENTER VALID LOGINID AND PASSWORD!!!";
	public static final String LOGIN_SUCCESS = "LOGGED IN SUCCESSFULLY!!!";
	public static final String PROPER_INITIAL_AMOUNT = "INITIAL COST SHOULD BE 20% OF THE PROPERTY COST!!!";
	public static final String PROPER_EMI_AMOUNT = "EMI AMOUNT SHOULD BE LESS THAN OR EQUAL TO SALARY!!!";
	public static final String TENSURE_VALID = "TENURE SHOULD BE MINIMUM 12 MONTHS";
	public static final String EMI_CALC = "YOUR EMI PER MONTH IS ";
	public static final String AUTO_DEBIT_MONTHLY_SUCCESS_MESSAGE = "AUTO DEBIT MONTHLY SUCCESSFULL!!!";
	public static final String AUTO_DEBIT_MONTHLY_FAILED_MESSAGE = "AUTO DEBIT MONTHLY FAILED!!!";
	public static final String ACCOUNT_NOT_REGISTERED = "ACCOUNT NUMBER IS NOT PRESENT!!!";
}
