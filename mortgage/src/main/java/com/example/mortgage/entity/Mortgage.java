package com.example.mortgage.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
@Entity
public class Mortgage {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer mortgageId;
	private Double propertyCost;
	private Double initialDeposit;
	private String operationType;
	private Integer tenure;
	private Double emiAmount;
	private Double rateOfInterest;
	private Integer customerId;


}
