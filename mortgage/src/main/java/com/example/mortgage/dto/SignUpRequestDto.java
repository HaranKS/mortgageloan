package com.example.mortgage.dto;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SignUpRequestDto {

	@NotEmpty(message = "ENTER CUSTOMER NAME!!!")
	@Size(min = 4, max = 30)
	private String customerName;
	private LocalDate dob;
	@NotEmpty(message = "Email should not be empty")
	@Email(message = "Email should be valid")
	@Pattern(regexp = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\." + "[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
			+ "(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", 
			message = "GIVE PROPER FORMAT OF EMAIL!!!")
	private String emailId;
	private Double salary;
	@NotEmpty(message = "PAN NUMBER SHOULD NOT BE EMPTY!!!")
	private String panNumber;
	@NotEmpty(message = "OCCUPATION SHOULD NOT BE EMPTY!!!")
	private String occupation;
	@NotEmpty(message = "EMPLOYEE STATUS SHOULD NOT BE EMPTY!!!")
	private String employementStatus;
	private Double propertyCost;
	private Double initialDeposit;
	@NotEmpty(message = "OPERATION TYPE SHOULD NOT BE EMPTY!!!")
	private String operationType;
	@NotNull
	private Integer tenure;
	private Double emiAmount;
	private Double rateOfInterest;

}
