package com.example.mortgage.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginRequestDto {
	@NotEmpty	
	@Size(min = 6, max = 30)
	private String password;
	@NotEmpty(message = "ENTER THE LOGINID!!!")
	private String loginId;

}
