package com.example.mortgage.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmiRequestDto {

	private Double propertyCost;
	private Double initialDeposit;
	private Double rateOfInterest;
	@NotNull
	private Integer tenure;
}
