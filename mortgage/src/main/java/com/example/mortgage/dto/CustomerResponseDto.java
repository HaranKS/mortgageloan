package com.example.mortgage.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerResponseDto {
	
	private String password;
	private String loginId;

}
