package com.example.mortgage.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountDto {

	@NotNull
	private Long accountNumber;
	@NotEmpty(message = "ENTER THE ACCOUNT TYPE!!!")
	private String accountType;
	private Double balance;

}
